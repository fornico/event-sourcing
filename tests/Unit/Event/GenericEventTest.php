<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Event;

use Zisato\EventSourcing\Event\GenericEvent;
use PHPUnit\Framework\TestCase;

class GenericEventTest extends TestCase
{
    public function testItShouldCreateSuccessfully(): void
    {
        $model = new GenericEvent(
            $createdAt = new \DateTimeImmutable(),
            $payload = []
        );

        $this->assertEquals($createdAt, $model->createdAt());
        $this->assertEquals($payload, $model->payload());
    }
}
