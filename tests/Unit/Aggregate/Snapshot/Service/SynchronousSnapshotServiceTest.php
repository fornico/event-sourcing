<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Snapshot\Service;

use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Aggregate\Snapshot\GenericSnapshot;
use Zisato\EventSourcing\Aggregate\Snapshot\Service\SynchronousSnapshotService;
use Zisato\EventSourcing\Aggregate\Snapshot\Store\SnapshotStore;
use Zisato\EventSourcing\Tests\Unit\Aggregate\AggregateFactoryTrait;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class SynchronousSnapshotServiceTest extends TestCase
{
    use AggregateFactoryTrait;
    
    public function testItShouldCreateSucessfully(): void
    {
        /** @var SnapshotStore|MockObject $snapshoStore */
        $snapshoStore = $this->createMock(SnapshotStore::class);
        $aggregateRoot = $this->createPerson(UUID::generate());
        $snapshot = GenericSnapshot::create($aggregateRoot, new \DateTimeImmutable());

        $service = new SynchronousSnapshotService($snapshoStore);

        $snapshoStore->expects($this->once())
            ->method('save')
            ->with($this->equalTo($snapshot));

        $service->create($snapshot);
    }
}
