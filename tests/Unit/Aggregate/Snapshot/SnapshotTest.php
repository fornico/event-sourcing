<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Snapshot;

use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Aggregate\Snapshot\GenericSnapshot;
use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Person;
use Zisato\EventSourcing\Tests\Unit\Aggregate\AggregateFactoryTrait;
use PHPUnit\Framework\TestCase;

class SnapshotTest extends TestCase
{
    use AggregateFactoryTrait;
    
    public function testItShouldCreateSucessfully(): void
    {
        $aggregateId = UUID::generate();
        $aggregateRoot = $this->createPerson($aggregateId);
        $aggregateRootVersion = Version::create(1);
        $createdAt = new \DateTimeImmutable();
        $snapshot = GenericSnapshot::create($aggregateRoot, $createdAt);

        $this->assertEquals($snapshot->aggregateRoot(), $aggregateRoot);
        $this->assertEquals($snapshot->aggregateRootId(), $aggregateId);
        $this->assertEquals($snapshot->aggregateRootVersion(), $aggregateRootVersion);
        $this->assertEquals($snapshot->aggregateRootClassName(), Person::class);
        $this->assertEquals($snapshot->createdAt(), $createdAt);
    }
}
