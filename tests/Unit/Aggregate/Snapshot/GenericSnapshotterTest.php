<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Snapshot;

use Zisato\EventSourcing\Aggregate\AggregateRoot;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Aggregate\Snapshot\GenericSnapshot;
use Zisato\EventSourcing\Aggregate\Snapshot\GenericSnapshotter;
use Zisato\EventSourcing\Aggregate\Snapshot\Service\SnapshotService;
use Zisato\EventSourcing\Aggregate\Snapshot\Snapshot;
use Zisato\EventSourcing\Aggregate\Snapshot\Snapshotter;
use Zisato\EventSourcing\Aggregate\Snapshot\Store\SnapshotStore;
use Zisato\EventSourcing\Aggregate\Snapshot\Strategy\SnapshotStrategy;
use Zisato\EventSourcing\Identity\Identity;
use Zisato\EventSourcing\Tests\Unit\Aggregate\AggregateFactoryTrait;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GenericSnapshotterTest extends TestCase
{
    use AggregateFactoryTrait;
    
    private Snapshotter $snapshotter;
    /** @var SnapshotStore|MockObject $snapshotStore */
    private $snapshotStore;
    /** @var SnapshotStrategy|MockObject $snapshotStrategy */
    private $snapshotStrategy;
    /** @var SnapshotService|MockObject $snapshotService */
    private $snapshotService;

    protected function setUp(): void
    {
        $this->snapshotStore = $this->createMock(SnapshotStore::class);
        $this->snapshotStrategy = $this->createMock(SnapshotStrategy::class);
        $this->snapshotService = $this->createMock(SnapshotService::class);
        
        $this->snapshotter = new GenericSnapshotter(
            $this->snapshotStore,
            $this->snapshotStrategy,
            $this->snapshotService
        );
    }

    /**
     * @dataProvider getGetSuccessData
     */
    public function testItShouldGetSucessfully(
        Identity $aggregateId,
        ?Snapshot $snapshot,
        ?AggregateRoot $expected
    ): void {
        $this->snapshotStore->expects($this->once())
            ->method('get')
            ->with($this->equalTo($aggregateId))
            ->willReturn($snapshot);

        $aggregate = $this->snapshotter->get($aggregateId);

        $this->assertEquals($expected, $aggregate);
    }

    public function testItShoulNotCreateWhenStrategyReturnFalse(): void
    {
        $aggregateId = UUID::generate();

        $aggregate = $this->createPerson($aggregateId);

        $this->snapshotStrategy->expects($this->once())
            ->method('shouldCreateSnapshot')
            ->with($this->equalTo($aggregate))
            ->willReturn(false);

        $this->snapshotService->expects($this->never())
            ->method('create');

        $this->snapshotter->handle($aggregate);
    }

    public function testItShoultCreateWhenStrategyReturnTrue(): void
    {
        $aggregateId = UUID::generate();

        $aggregate = $this->createPerson($aggregateId);

        $this->snapshotStrategy->expects($this->once())
            ->method('shouldCreateSnapshot')
            ->with($this->equalTo($aggregate))
            ->willReturn(true);

        $this->snapshotService->expects($this->once())
            ->method('create');

        $this->snapshotter->handle($aggregate);
    }

    public function getGetSuccessData(): array
    {
        $aggregateId = UUID::generate();

        return [
            [
                $aggregateId,
                $snapshot = GenericSnapshot::create(
                    $this->createPerson($aggregateId), 
                    new \DateTimeImmutable()
                ),
                $snapshot->aggregateRoot(),
            ],
            [
                $aggregateId,
                null,
                null,
            ]
        ];
    }
}
