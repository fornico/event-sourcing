<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Repository;

use Zisato\EventSourcing\Aggregate\Event\Collector\EventCollector;
use Zisato\EventSourcing\Aggregate\Event\Decorator\EventDecorator;
use Zisato\EventSourcing\Aggregate\Event\Store\EventStore;
use Zisato\EventSourcing\Aggregate\Event\Stream\GeneratorEventStream;
use Zisato\EventSourcing\Aggregate\Exception\AggregateRootNotFoundException;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Aggregate\Repository\AggregateRootRepository;
use Zisato\EventSourcing\Aggregate\Repository\GenericAggregateRootRepository;
use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Person;
use Zisato\EventSourcing\Tests\Stub\Aggregate\ValueObject\StringVO;
use Zisato\EventSourcing\Tests\Unit\Aggregate\AggregateFactoryTrait;
use Zisato\EventSourcing\Tests\Unit\Aggregate\Event\EventFactoryTrait;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GenericAggregateRootRepositoryTest extends TestCase
{
    use EventFactoryTrait, AggregateFactoryTrait;
    
    private AggregateRootRepository $repository;
    /** @var EventStore|MockObject $eventStore */
    private $eventStore;

    protected function setUp(): void
    {
        $this->eventStore = $this->createMock(EventStore::class);

        $this->repository = new GenericAggregateRootRepository(
            Person::class,
            $this->eventStore,
            null,
            null
        );
    }

    public function testItShouldGetSuccessfully(): void
    {
        $aggregateId = UUID::generate();

        $event = $this->createPersonCreatedEvent($aggregateId);
        $event = $event->withAggregateVersion(Version::create(1));

        $eventStream = GeneratorEventStream::create();
        $eventStream->add($event);

        $expectedResult = Person::reconstitute($aggregateId, $eventStream);

        $this->eventStore->expects($this->once())
            ->method('get')
            ->with($this->equalTo($aggregateId))
            ->willReturn($eventStream);

        $result = $this->repository->get($aggregateId);

        $this->assertEquals($expectedResult, $result);
    }

    public function testItShouldSaveSuccessfully(): void
    {
        $aggregateId = UUID::generate();

        $aggregate = $this->createPerson($aggregateId);
        $aggregate->changeName(new StringVO('newName'));

        $this->eventStore->expects($this->exactly(2))
            ->method('save');

        $this->repository->save($aggregate);
    }

    public function testItShouldSaveWithEventDecoratorAndEventCollectorSuccessfully(): void
    {
        $aggregateId = UUID::generate();

        $aggregate = $this->createPerson($aggregateId);
        $aggregate->changeName(new StringVO('newName'));

        $this->eventStore->expects($this->exactly(2))
            ->method('save');

        $eventDecorator = $this->createMock(EventDecorator::class);
        $eventDecorator->expects($this->exactly(2))
            ->method('decorate');

        $eventCollector = $this->createMock(EventCollector::class);
        $eventCollector->expects($this->exactly(2))
            ->method('add');

        $repository = new GenericAggregateRootRepository(
            Person::class,
            $this->eventStore,
            $eventDecorator,
            $eventCollector
        );

        $repository->save($aggregate);
    }

    public function testItShouldThrowAggregateRootNotFoundException(): void
    {
        $this->expectException(AggregateRootNotFoundException::class);

        $aggregateId = UUID::generate();
        $eventStream = GeneratorEventStream::create();

        $this->eventStore->expects($this->once())
            ->method('get')
            ->with($this->equalTo($aggregateId))
            ->willReturn($eventStream);

        $this->repository->get($aggregateId);
    }
}
