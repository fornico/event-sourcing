<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Repository;

use Zisato\EventSourcing\Aggregate\Event\Store\EventStore;
use Zisato\EventSourcing\Aggregate\Event\Stream\GeneratorEventStream;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Aggregate\Repository\AggregateRootRepository;
use Zisato\EventSourcing\Aggregate\Repository\AggregateRootRepositoryWithSnapshot;
use Zisato\EventSourcing\Aggregate\Snapshot\Snapshotter;
use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Person;
use Zisato\EventSourcing\Tests\Stub\Aggregate\ValueObject\StringVO;
use Zisato\EventSourcing\Tests\Unit\Aggregate\AggregateFactoryTrait;
use Zisato\EventSourcing\Tests\Unit\Aggregate\Event\EventFactoryTrait;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class AggregateRootRepositoryWithSnapshotTest extends TestCase
{
    use EventFactoryTrait, AggregateFactoryTrait;
    
    private AggregateRootRepository $repository;
    /** @var EventStore|MockObject $eventStore */
    private $eventStore;
    /** @var Snapshotter|MockObject $snapshotter */
    private $snapshotter;

    protected function setUp(): void
    {
        $this->eventStore = $this->createMock(EventStore::class);
        $this->snapshotter = $this->createMock(Snapshotter::class);

        $this->repository = new AggregateRootRepositoryWithSnapshot(
            Person::class,
            $this->eventStore,
            $this->snapshotter,
            null,
            null
        );
    }

    public function testItShouldGetFromSnapshotSuccessfully(): void
    {
        $aggregateId = UUID::generate();

        $eventPersonCreated = $this->createPersonCreatedEvent($aggregateId);
        $eventPersonCreated = $eventPersonCreated->withAggregateVersion(Version::create(1));

        $eventStream1 = GeneratorEventStream::create();
        $eventStream1->add($eventPersonCreated);

        $eventPersonNameChanged = $this->createPersonNameChangedEvent($aggregateId);
        $eventPersonNameChanged = $eventPersonNameChanged->withAggregateVersion(Version::create(2));

        $eventStream2 = GeneratorEventStream::create();
        $eventStream2->add($eventPersonNameChanged);
        
        $aggregate = Person::reconstitute($aggregateId, $eventStream1);

        $expectedResult = Person::reconstitute($aggregateId, $eventStream1);
        $expectedResult->replyEvents($eventStream2);

        $this->snapshotter->expects($this->once())
            ->method('get')
            ->with($this->equalTo($aggregateId))
            ->willReturn($aggregate);

        $this->eventStore->expects($this->once())
            ->method('get')
            ->with($this->equalTo($aggregateId))
            ->willReturn($eventStream2);

        $result = $this->repository->get($aggregateId);

        $this->assertEquals($expectedResult, $result);
    }

    public function testItShouldGetFromParentSuccessfully(): void
    {
        $aggregateId = UUID::generate();

        $eventPersonCreated = $this->createPersonCreatedEvent($aggregateId);
        $eventPersonCreated = $eventPersonCreated->withAggregateVersion(Version::create(1));
        
        $eventPersonNameChanged = $this->createPersonNameChangedEvent($aggregateId);
        $eventPersonNameChanged = $eventPersonNameChanged->withAggregateVersion(Version::create(2));

        $eventStream = GeneratorEventStream::create();
        $eventStream->add($eventPersonCreated);
        $eventStream->add($eventPersonNameChanged);

        $expectedResult = Person::reconstitute($aggregateId, $eventStream);

        $this->snapshotter->expects($this->once())
            ->method('get')
            ->with($this->equalTo($aggregateId))
            ->willReturn(null);

        $this->eventStore->expects($this->once())
            ->method('get')
            ->with($this->equalTo($aggregateId))
            ->willReturn($eventStream);

        $result = $this->repository->get($aggregateId);

        $this->assertEquals($expectedResult, $result);
    }

    public function testItShouldSaveSnapshotSuccessfully(): void
    {
        $aggregateId = UUID::generate();

        $aggregate = $this->createPerson($aggregateId);
        $aggregate->changeName(new StringVO('newName'));
        
        $this->eventStore->expects($this->exactly(2))
            ->method('save');

        $this->snapshotter->expects($this->once())
            ->method('handle')
            ->with($this->equalTo($aggregate));

        $this->repository->save($aggregate);
    }
}
