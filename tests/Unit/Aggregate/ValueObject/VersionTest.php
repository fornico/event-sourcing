<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\ValueObject;

use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use PHPUnit\Framework\TestCase;

class VersionTest extends TestCase
{
    /**
     * @dataProvider getValidData
     */
    public function testItShouldCreateSuccessfully(int $value): void
    {
        $model = Version::create($value);

        $this->assertEquals($value, $model->value());
    }

    /**
     * @dataProvider getInvalidData
     */
    public function testItShouldThrowInvalidArgumentExceptionWhenInvalidArgument(int $value): void
    {
        $this->expectException(\InvalidArgumentException::class);

        Version::create($value);
    }

    /**
     * @dataProvider getNextData
     */
    public function testItShouldReturnNextSuccessfully(int $current, int $expected): void
    {
        $model = Version::create($current);

        $this->assertEquals($expected, $model->next()->value());
    }

    /**
     * @dataProvider getEqualsData
     */
    public function testItShouldReturnEqualsSuccessfully(int $current, int $next, bool $expected): void
    {
        $currentVersion = Version::create($current);
        $nextVersion = Version::create($next);

        $this->assertEquals($currentVersion->equals($nextVersion), $expected);
    }

    public function getValidData(): array
    {
        return [
            [0],
            [1],
            [2],
        ];
    }

    public function getInvalidData(): array
    {
        return [
            [-1],
        ];
    }

    public function getNextData(): array
    {
        return [
            [0, 1],
            [4, 5],
            [100, 101],
        ];
    }

    public function getEqualsData(): array
    {
        return [
            [0, 1, false],
            [5, 4, false],
            [1, 1, true],
            [42, 42, true],
        ];
    }
}
