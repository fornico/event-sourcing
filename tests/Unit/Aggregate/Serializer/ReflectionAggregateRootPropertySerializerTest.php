<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Serializer;

use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Aggregate\Serializer\ReflectionAggregateRootPropertySerializer;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Person;
use Zisato\EventSourcing\Tests\Stub\Aggregate\ValueObject\IntegerVO;
use Zisato\EventSourcing\Tests\Stub\Aggregate\ValueObject\StringVO;
use Zisato\EventSourcing\Tests\Unit\Aggregate\AggregateFactoryTrait;
use PHPUnit\Framework\TestCase;

class ReflectionAggregateRootPropertySerializerTest extends TestCase
{
    use AggregateFactoryTrait;
    
    private ReflectionAggregateRootPropertySerializer $serializer;

    protected function setUp(): void
    {
        $this->serializer = new ReflectionAggregateRootPropertySerializer();
    }

    public function testItShouldIgnoreNotExistingPropertyAggregateRootInData(): void
    {
        $aggregateId = UUID::fromString('e21f9b3c-8446-11eb-855e-0242ac120002');
        $aggregateRoot = $this->createPerson($aggregateId);
        
        /** @var \ReflectionClass<AggregateRoot> $class */
        $class = new \ReflectionClass($aggregateRoot);

        $expectedName = new StringVO('name');
        $expectedAge = new IntegerVO(42);

        $propertiesValues = [
            'name' => $expectedName,
            'age' => $expectedAge,
            'nonExisting' => 'value'
        ];

        $this->serializer->setProperties($aggregateRoot, $class, $propertiesValues);

        $this->assertClassNotHasAttribute('nonExisting', Person::class);
        $this->assertEquals($expectedName, $aggregateRoot->name());
        $this->assertEquals($expectedAge, $aggregateRoot->age());
    }
}
