<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate;

use Zisato\EventSourcing\Aggregate\Event\Stream\GeneratorEventStream;
use Zisato\EventSourcing\Aggregate\Exception\AggregateReconstituteException;
use Zisato\EventSourcing\Aggregate\Exception\InvalidAggregateVersionException;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Person;
use Zisato\EventSourcing\Tests\Unit\Aggregate\AggregateFactoryTrait;
use Zisato\EventSourcing\Tests\Unit\Aggregate\Event\EventFactoryTrait;
use PHPUnit\Framework\TestCase;

class AggregateRootTest extends TestCase
{
    use AggregateFactoryTrait, EventFactoryTrait;
    
    public function testItShouldReconstituteSuccessfully(): void
    {
        $aggregateId = UUID::generate();
        $aggregate = $this->createPerson($aggregateId);

        $eventStream = $aggregate->releaseRecordedEvents();

        $expected = Person::reconstitute($aggregateId, $eventStream);

        $this->assertEquals($expected, $aggregate);
    }

    public function testItShouldThrowInvalidArgumentExceptionWhenEmptyEventStream(): void
    {
        $this->expectException(AggregateReconstituteException::class);

        $aggregateId = UUID::generate();
        $eventStream = GeneratorEventStream::create();

        Person::reconstitute($aggregateId, $eventStream);
    }

    public function testItShouldThrowInvalidArgumentExceptionWhenApplyEventAggregateVersionLowerThanAggregateVersion(): void
    {
        $this->expectException(InvalidAggregateVersionException::class);

        $aggregateId = UUID::generate();
        $eventStream = GeneratorEventStream::create();
        $event = $this->createPersonCreatedEvent($aggregateId);

        $eventStream->add($event);

        $aggregate = $this->createPerson($aggregateId);

        $aggregate->replyEvents($eventStream);
    }
}
