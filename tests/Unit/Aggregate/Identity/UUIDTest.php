<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Identity;

use Zisato\EventSourcing\Aggregate\Identity\UUID;
use PHPUnit\Framework\TestCase;

class UUIDTest extends TestCase
{
    /**
     * @dataProvider getValidStringData
     */
    public function testItShouldCreateFromStringSuccessfully(string $value): void
    {
        $model = UUID::fromString($value);

        $this->assertEquals($value, $model->value());
    }

    public function getValidStringData(): array
    {
        return [
            ['33293656-6e4d-11eb-9439-0242ac130002'],
            ['961ee66a-9f99-3346-b870-c34eb90e6235'],
            ['a42cde30-aec8-45a7-ae2a-63488503fe17'],
        ];
    }

    public function getInvalidData(): array
    {
        return [
            [''],
            ['00000000-0000-0000-0000-000000000000'],
        ];
    }
}
