<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Identity\Factory;

use Zisato\EventSourcing\Aggregate\Identity\Factory\UUIDFactory;
use PHPUnit\Framework\TestCase;

class UUIDFactoryTest extends TestCase
{
    /**
     * @dataProvider getValidStringData
     */
    public function testItShouldCreateFromStringSuccessfully(string $value): void
    {
        $factory = new UUIDFactory();
        $identity = $factory->fromString($value);

        $this->assertEquals($value, $identity->value());
    }

    public function getValidStringData(): array
    {
        return [
            ['33293656-6e4d-11eb-9439-0242ac130002'],
            ['961ee66a-9f99-3346-b870-c34eb90e6235'],
            ['a42cde30-aec8-45a7-ae2a-63488503fe17'],
        ];
    }
}
