<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate;

use Zisato\EventSourcing\Identity\Identity;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Person;
use Zisato\EventSourcing\Tests\Stub\Aggregate\ValueObject\IntegerVO;
use Zisato\EventSourcing\Tests\Stub\Aggregate\ValueObject\StringVO;

trait AggregateFactoryTrait
{
    private static string $DEFAULT_NAME = 'defaultName';
    private static string $DEFAULT_EMAIL = 'default@email.com';
    private static string $DEFAULT_PREVIOUS_NAME = 'previousName';
    private static string $DEFAULT_NEW_NAME = 'newName';

    public function createPerson(
        Identity $aggregateId, 
        ?string $name = null, 
        ?string $email = null,
        ?string $phone = null,
        ?int $age = null
    ): Person {
        if ($name === null) {
            $name = self::$DEFAULT_NAME;
        }
        
        if ($email === null) {
            $email = self::$DEFAULT_EMAIL;
        }
        
        if ($phone !== null) {
            $phone = new StringVO($phone);
        }
        
        if ($age !== null) {
            $age = new IntegerVO($age);
        }

        return Person::create(
            $aggregateId,
            new StringVO($name), 
            new StringVO($email),
            $phone,
            $age
        );
    }
}
