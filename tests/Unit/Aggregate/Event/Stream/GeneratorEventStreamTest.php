<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Event\Stream;

use Zisato\EventSourcing\Aggregate\Event\Stream\GeneratorEventStream;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Tests\Unit\Aggregate\Event\EventFactoryTrait;
use PHPUnit\Framework\TestCase;

class GeneratorEventStreamTest extends TestCase
{
    use EventFactoryTrait;
    
    /**
     * @dataProvider getData
     */
    public function testItShouldAddSuccessfully(array $events): void
    {
        $eventStream = GeneratorEventStream::create();

        foreach ($events as $event) {
            $eventStream->add($event);
        }

        $expectedCount = \count($events);
        $index = 0;

        $this->assertEquals($expectedCount, $eventStream->count());

        foreach ($eventStream->events() as $event) {
            $this->assertEquals($events[$index], $event);

            $index++;
        }
    }

    public function getData(): array
    {
        $aggregateId = UUID::generate();

        return [
            [
                []
            ],
            [
                [
                    $this->createPersonCreatedEvent($aggregateId),
                ]
            ],
            [
                [
                    $this->createPersonCreatedEvent($aggregateId),
                    $this->createPersonCreatedEvent($aggregateId),
                ]
            ],
            [
                [
                    $this->createPersonCreatedEvent($aggregateId),
                    $this->createPersonCreatedEvent($aggregateId),
                    $this->createPersonCreatedEvent($aggregateId),
                ]
            ],
        ];
    }
}
