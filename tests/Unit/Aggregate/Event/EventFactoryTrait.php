<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Event;

use Zisato\EventSourcing\Identity\Identity;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonCreated;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonNameChanged;

trait EventFactoryTrait
{
    private static string $DEFAULT_NAME = 'defaultName';
    private static string $DEFAULT_EMAIL = 'default@email.com';
    private static string $DEFAULT_PREVIOUS_NAME = 'previousName';
    private static string $DEFAULT_NEW_NAME = 'newName';

    public function createPersonCreatedEvent(
        Identity $aggregateId, 
        ?string $name = null, 
        ?string $email = null,
        ?string $phone = null,
        ?int $age = null
    ): PersonCreated {
        if ($name === null) {
            $name = self::$DEFAULT_NAME;
        }
        
        if ($email === null) {
            $email = self::$DEFAULT_EMAIL;
        }

        return PersonCreated::create($aggregateId, $name, $email, $phone, $age);
    }

    public function createPersonNameChangedEvent(
        Identity $aggregateId, 
        ?string $previousName = null, 
        ?string $newName = null
    ): PersonNameChanged {
        if ($previousName === null) {
            $previousName = self::$DEFAULT_PREVIOUS_NAME;
        }
        
        if ($newName === null) {
            $newName = self::$DEFAULT_NEW_NAME;
        }

        return PersonNameChanged::create($aggregateId, $previousName, $newName);
    }
}
