<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Event\Collector;

use Zisato\EventSourcing\Aggregate\Event\Collector\InMemoryEventCollector;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Tests\Unit\Aggregate\Event\EventFactoryTrait;
use PHPUnit\Framework\TestCase;

class InMemoryEventCollectorTest extends TestCase
{
    use EventFactoryTrait;

    /**
     * @dataProvider getData
     */
    public function testItShouldPushAndReleaseSuccessfully(array $events): void
    {
        $eventCollector = new InMemoryEventCollector();

        foreach ($events as $event) {
            $eventCollector->add($event);
        }

        $expectedTotal = \count($events);
        $index = 0;

        foreach ($eventCollector->release() as $event) {
            $this->assertEquals($events[$index], $event);

            $index++;
        }

        $this->assertEquals($expectedTotal, $index);
    }

    public function getData(): array
    {
        $aggregateId = UUID::generate();

        return [
            [
                []
            ],
            [
                [
                    $this->createPersonCreatedEvent($aggregateId),
                ]
            ],
            [
                [
                    $this->createPersonCreatedEvent($aggregateId),
                    $this->createPersonCreatedEvent($aggregateId),
                ]
            ],
            [
                [
                    $this->createPersonCreatedEvent($aggregateId),
                    $this->createPersonCreatedEvent($aggregateId),
                    $this->createPersonCreatedEvent($aggregateId),
                ]
            ],
        ];
    }
}
