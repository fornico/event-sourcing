<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Event\Collector;

use Zisato\EventSourcing\Aggregate\Event\Collector\NullEventCollector;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Tests\Unit\Aggregate\Event\EventFactoryTrait;
use PHPUnit\Framework\TestCase;

class NullEventCollectorTest extends TestCase
{
    use EventFactoryTrait;
    
    public function testItShouldPushAndReleaseSuccessfully(): void
    {
        $aggregateId = UUID::generate();
        $eventCollector = new NullEventCollector();
        $expectedTotal = 0;
        $index = 0;

        $eventCollector->add($this->createPersonCreatedEvent($aggregateId));

        foreach ($eventCollector->release() as $event) {
            $index++;
        }

        $this->assertEquals($expectedTotal, $index);
    }
}
