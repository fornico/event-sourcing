<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Event;

use Zisato\EventSourcing\Aggregate\Event\EventArgumentMethodCaller;
use Zisato\EventSourcing\Aggregate\Exception\MethodNotDefinedException;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonCreated;
use Zisato\EventSourcing\Tests\Unit\Aggregate\Event\EventFactoryTrait;
use PHPUnit\Framework\TestCase;

class EventArgumentMethodCallerTest extends TestCase
{
    use EventFactoryTrait;
    
    public function testItShouldCallSuccessfully(): void
    {
        $trait = new class {
            use EventArgumentMethodCaller;

            private array $eventPayload;

            public function eventPayload(): array
            {
                return $this->eventPayload;
            }

            protected function applyPersonCreated(PersonCreated $event): void
            {
                $this->eventPayload = $event->payload();
            }
        };

        $event = $this->createPersonCreatedEvent(UUID::generate());

        $trait->callMethod($event, 'apply');

        $this->assertEquals($event->payload(), $trait->eventPayload());
    }

    public function testItShouldThrowInvalidArgumentExceptionWhenMethodNotExists(): void
    {
        $this->expectException(MethodNotDefinedException::class);

        $trait = new class {
            use EventArgumentMethodCaller;
        };

        $trait->callMethod($this->createPersonCreatedEvent(UUID::generate()), 'apply');
    }
}
