<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Event\Serializer;

use Zisato\EventSourcing\Aggregate\Event\Serializer\EventSerializer;
use Zisato\EventSourcing\Aggregate\Event\Serializer\UpcasterEventSerializer;
use Zisato\EventSourcing\Aggregate\Event\Upcast\Upcaster;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonCreated;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class UpcasterEventSerializerTest extends TestCase
{
    /** @var EventSerializer|MockObject $eventSerializer */
    private $eventSerializer;
    /** @var MockObject|Upcaster $upcaster */
    private $upcaster;
    private UpcasterEventSerializer $upcasterEventSerializer;

    protected function setUp(): void
    {
        $this->eventSerializer = $this->createMock(EventSerializer::class);
        $this->upcaster = $this->createMock(Upcaster::class);

        $this->upcasterEventSerializer = new UpcasterEventSerializer(
            $this->eventSerializer,
            $this->upcaster
        );
    }

    public function testItShouldCreateFromArraySuccessfully(): void
    {
        $aggregateId = UUID::generate();

        $data = [
            'event_class' => PersonCreated::class,
            'aggregate_id' => $aggregateId->value(),
            'aggregate_version' => Version::create(1)->value(),
            'created_at' => (new \DateTimeImmutable())->format(UpcasterEventSerializer::DATE_FORMAT),
            'payload' => json_encode([]),
            'version' => 1,
            'metadata' => json_encode(['key' => 'value'])
        ];

        $event = $this->createMock(PersonCreated::class);

        $eventUpcasted = PersonCreated::reconstitute(
            $aggregateId,
            $aggregateVersion = Version::create(2),
            $createdAt = new \DateTimeImmutable(),
            $payload = ['foo' => 'bar'],
            $version = 2,
            $metadata = ['key' => 'value']
        );

        $this->eventSerializer->expects($this->once())
            ->method('fromArray')
            ->with($this->equalTo($data))
            ->willReturn($event);

        $this->upcaster->expects($this->once())
            ->method('canUpcast')
            ->with($this->equalTo($event))
            ->willReturn(true);

        $this->upcaster->expects($this->once())
            ->method('upcast')
            ->with($this->equalTo($event))
            ->willReturn($eventUpcasted);

        $result = $this->upcasterEventSerializer->fromArray($data);

        $this->assertEquals($aggregateId, $result->aggregateId());
        $this->assertEquals($aggregateVersion, $result->aggregateVersion());
        $this->assertEquals($createdAt, $result->createdAt());
        $this->assertEquals($version, $result->version());
        $this->assertEquals($payload, $result->payload());
        $this->assertEquals($metadata, $result->metadata());
    }

    public function testItShouldReturnAsArraySuccessfully(): void
    {
        $aggregateId = UUID::generate();

        $data = [
            'event_class' => PersonCreated::class,
            'aggregate_id' => $aggregateId->value(),
            'aggregate_version' => Version::create(1)->value(),
            'created_at' => (new \DateTimeImmutable())->format(UpcasterEventSerializer::DATE_FORMAT),
            'payload' => json_encode([]),
            'version' => 1,
            'metadata' => json_encode(['key' => 'value'])
        ];

        $event = $this->createMock(PersonCreated::class);

        $this->eventSerializer->expects($this->once())
            ->method('toArray')
            ->with($this->equalTo($event))
            ->willReturn($data);

        $this->upcasterEventSerializer->toArray($event);
    }
}
