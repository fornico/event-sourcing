<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Event\Serializer;

use Zisato\EventSourcing\Aggregate\Event\Serializer\JSONPayloadSerializer;
use Zisato\EventSourcing\Aggregate\Event\Serializer\PayloadSerializer;
use PHPUnit\Framework\TestCase;

class JSONPayloadSerializerTest extends TestCase
{
    private PayloadSerializer $payloadSerializer;

    protected function setUp(): void
    {
        $this->payloadSerializer = new JSONPayloadSerializer();
    }

    public function testItShouldSerializeSuccessfully(): void
    {
        $payload = [
            'foo' => 'bar',
            'jhon' => [
                'doe' => 1,
            ]
        ];

        $result = $this->payloadSerializer->serialize($payload);

        $this->assertEquals($result, \json_encode($payload, \JSON_UNESCAPED_UNICODE));
    }

    public function testItShouldDeserializeSuccessfully(): void
    {
        $payload = '{"foo":"bar","jhon":{"doe":1}}';

        $result = $this->payloadSerializer->deserialize($payload);

        $this->assertEquals($result, \json_decode($payload, true));
    }
}
