<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Event\Serializer;

use Zisato\EventSourcing\Aggregate\Event\Serializer\JSONMetadataSerializer;
use Zisato\EventSourcing\Aggregate\Event\Serializer\MetadataSerializer;
use PHPUnit\Framework\TestCase;

class JSONMetadataSerializerTest extends TestCase
{
    private MetadataSerializer $metadataSerializer;

    protected function setUp(): void
    {
        $this->metadataSerializer = new JSONMetadataSerializer();
    }

    public function testItShouldSerializeSuccessfully(): void
    {
        $payload = [
            'foo' => 'bar',
            'jhon' => [
                'doe' => 1,
            ]
        ];

        $result = $this->metadataSerializer->serialize($payload);

        $this->assertEquals($result, \json_encode($payload, \JSON_UNESCAPED_UNICODE));
    }

    public function testItShouldDeserializeSuccessfully(): void
    {
        $payload = '{"foo":"bar","jhon":{"doe":1}}';

        $result = $this->metadataSerializer->deserialize($payload);

        $this->assertEquals($result, \json_decode($payload, true));
    }
}
