<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Event\Serializer;

use Zisato\EventSourcing\Aggregate\Event\Serializer\EventSerializer;
use Zisato\EventSourcing\Aggregate\Event\Serializer\GenericEventSerializer;
use Zisato\EventSourcing\Aggregate\Event\Serializer\MetadataSerializer;
use Zisato\EventSourcing\Aggregate\Event\Serializer\PayloadSerializer;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Identity\Factory\IdentityFactory;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonCreated;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GenericEventSerializerTest extends TestCase
{
    /** @var IdentityFactory|MockObject $identityFactory */
    private $identityFactory;
    /** @var PayloadSerializer|MockObject $payloadSerializer */
    private $payloadSerializer;
    /** @var MetadataSerializer|MockObject $metadataSerializer */
    private $metadataSerializer;
    private GenericEventSerializer $eventSerializer;

    protected function setUp(): void
    {
        $this->identityFactory = $this->createMock(IdentityFactory::class);
        $this->payloadSerializer = $this->createMock(PayloadSerializer::class);
        $this->metadataSerializer = $this->createMock(MetadataSerializer::class);
        $this->eventSerializer = new GenericEventSerializer(
            $this->identityFactory,
            $this->payloadSerializer,
            $this->metadataSerializer
        );
    }

    public function testItShouldCreateFromArraySuccessfully(): void
    {
        $aggregateId = UUID::generate();
        $aggregateVersion = Version::create(1);
        $createdAt = new \DateTimeImmutable();
        $payload = [];
        $metadata = [];

        $data = [
            'event_class' => PersonCreated::class,
            'aggregate_id' => $aggregateId->value(),
            'aggregate_version' => $aggregateVersion->value(),
            'created_at' => $createdAt->format(EventSerializer::DATE_FORMAT),
            'payload' => \json_encode($payload),
            'version' => $version = 1,
            'metadata' => \json_encode($metadata)
        ];

        $this->identityFactory->expects($this->once())
            ->method('fromString')
            ->with($this->equalTo($data['aggregate_id']))
            ->willReturn($aggregateId);

        $this->payloadSerializer->expects($this->once())
            ->method('deserialize')
            ->with($this->equalTo($data['payload']))
            ->willReturn(\json_decode($data['payload'], true));

        $this->metadataSerializer->expects($this->once())
            ->method('deserialize')
            ->with($this->equalTo($data['metadata']))
            ->willReturn(\json_decode($data['metadata'], true));

        $result = $this->eventSerializer->fromArray($data);

        $this->assertEquals($aggregateId, $result->aggregateId());
        $this->assertEquals($aggregateVersion, $result->aggregateVersion());
        $this->assertEquals($createdAt, $result->createdAt());
        $this->assertEquals($payload, $result->payload());
        $this->assertEquals($version, $result->version());
        $this->assertEquals($metadata, $result->metadata());
    }

    public function testItShouldReturnAsArraySuccessfully(): void
    {
        $event = PersonCreated::reconstitute(
            $aggregateId = UUID::generate(),
            $aggregateVersion = Version::create(1),
            $createdAt = new \DateTimeImmutable(),
            $payload = [],
            $version = 1,
            $metadata = []
        );

        $this->payloadSerializer->expects($this->once())
            ->method('serialize')
            ->with($this->equalTo($payload))
            ->willReturn(\json_encode($payload));

        $this->metadataSerializer->expects($this->once())
            ->method('serialize')
            ->with($this->equalTo($metadata))
            ->willReturn(\json_encode($metadata));

        $result = $this->eventSerializer->toArray($event);

        $this->assertEquals(PersonCreated::class, $result['event_class']);
        $this->assertEquals($aggregateId->value(), $result['aggregate_id']);
        $this->assertEquals($aggregateVersion->value(), $result['aggregate_version']);
        $this->assertEquals($createdAt->format(EventSerializer::DATE_FORMAT), $result['created_at']);
        $this->assertEquals(\json_encode($payload), $result['payload']);
        $this->assertEquals($version, $result['version']);
        $this->assertEquals(\json_encode($metadata), $result['metadata']);
    }
}
