<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Event\Decorator;

use Zisato\EventSourcing\Aggregate\Event\Decorator\NullEventDecorator;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Tests\Unit\Aggregate\Event\EventFactoryTrait;
use PHPUnit\Framework\TestCase;

class NullEventDecoratorTest extends TestCase
{
    use EventFactoryTrait;
    
    public function testItShouldDecorateEventSuccessfully(): void
    {
        $aggregateId = UUID::generate();
        $event = $this->createPersonCreatedEvent($aggregateId);
        $eventDecorator = new NullEventDecorator();

        $result = $eventDecorator->decorate($event);

        $this->assertEquals($result, $event);
    }
}
