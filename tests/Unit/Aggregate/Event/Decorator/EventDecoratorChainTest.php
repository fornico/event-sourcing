<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Event\Decorator;

use Zisato\EventSourcing\Aggregate\Event\Decorator\EventDecorator;
use Zisato\EventSourcing\Aggregate\Event\Decorator\EventDecoratorChain;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Tests\Unit\Aggregate\Event\EventFactoryTrait;
use PHPUnit\Framework\TestCase;

class EventDecoratorChainTest extends TestCase
{
    use EventFactoryTrait;
    
    public function testItShouldDecorateEventSuccessfully(): void
    {
        $eventDecorator1 = $this->createMock(EventDecorator::class);
        $eventDecorator2 = $this->createMock(EventDecorator::class);

        $aggregateId = UUID::generate();
        $event = $this->createPersonCreatedEvent($aggregateId);
        $decoratedEvent1 = $event->addMetadata('foo', 'bar');
        $decoratedEvent2 = $event->addMetadata('bool', true)
            ->addMetadata('int', 42)
            ->addMetadata('float', 23.5)
            ->addMetadata('string', 'foo');

        $eventDecorator1->expects($this->once())
            ->method('decorate')
            ->with($this->equalTo($event))
            ->willReturn($decoratedEvent1);

        $eventDecorator2->expects($this->once())
            ->method('decorate')
            ->with($this->equalTo($decoratedEvent1))
            ->willReturn($decoratedEvent2);

        $eventDecorator = new EventDecoratorChain(
            $eventDecorator1,
            $eventDecorator2
        );

        $result = $eventDecorator->decorate($event);

        $this->assertEquals($result, $decoratedEvent2);
    }
}
