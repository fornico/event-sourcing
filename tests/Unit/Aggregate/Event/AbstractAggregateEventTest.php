<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Event;

use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonCreated;
use PHPUnit\Framework\TestCase;

class AbstractEventTest extends TestCase
{
    public function testItShouldCreateFromPayloadSuccessfully(): void
    {
        $model = PersonCreated::reconstitute(
            $aggregateId = UUID::generate(),
            $aggregateVersion = Version::create(1),
            $createdAt = new \DateTimeImmutable(),
            $payload = [
                'name' => 'personName',
                'email' => 'person@email.com',
                'phone' => '123456789',
            ],
            $version = 1,
            $metadata = [
                'foo' => 'bar'
            ]
        );

        $this->assertEquals($aggregateId, $model->aggregateId());
        $this->assertEquals($aggregateVersion, $model->aggregateVersion());
        $this->assertEquals($createdAt, $model->createdAt());
        $this->assertEquals($payload, $model->payload());
        $this->assertEquals($version, $model->version());
        $this->assertEquals($metadata, $model->metadata());
    }

    public function testItShouldAddMetadataSuccessfully(): void
    {
        $model = PersonCreated::reconstitute(
            $aggregateId = UUID::generate(),
            $aggregateVersion = Version::create(1),
            $createdAt = new \DateTimeImmutable(),
            $payload = [
                'name' => 'personName',
                'email' => 'person@email.com',
                'phone' => '123456789',
            ],
            $version = 1,
            []
        );

        $expectedMetadata = [
            'bool' => true,
            'int' => 42,
            'float' => 23.5,
            'string' => 'foo'
        ];
        foreach ($expectedMetadata as $key => $value) {
            $model = $model->addMetadata($key, $value);
        }

        $this->assertEquals($aggregateId, $model->aggregateId());
        $this->assertEquals($aggregateVersion, $model->aggregateVersion());
        $this->assertEquals($createdAt, $model->createdAt());
        $this->assertEquals($payload, $model->payload());
        $this->assertEquals($version, $model->version());
        $this->assertEquals($expectedMetadata, $model->metadata());
    }
}
