<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Unit\Aggregate\Event\Upcast;

use Zisato\EventSourcing\Aggregate\Event\Event;
use Zisato\EventSourcing\Aggregate\Event\Upcast\EventClassNameUpcasterChain;
use Zisato\EventSourcing\Aggregate\Event\Upcast\Upcaster;
use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonEmailChanged;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonPhoneChanged;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\Upcast\PersonEmailChangedFrom1To2Upcaster;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\Upcast\PersonPhoneChangedFrom1To2Upcaster;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\Upcast\PersonPhoneChangedFrom2To3Upcaster;
use PHPUnit\Framework\TestCase;

class EventClassNameUpcasterChainTest extends TestCase
{
    /**
     * @dataProvider getValidUpcastData
     */
    public function testItShouldUpcastSuccessfully(Event $event, array $expectedData, int $expectedVersion): void
    {
        $upcaster1 = new PersonPhoneChangedFrom1To2Upcaster();
        $upcaster2 = new PersonPhoneChangedFrom2To3Upcaster();
        $upcaster3 = new PersonEmailChangedFrom1To2Upcaster();

        $upcasterChain = new EventClassNameUpcasterChain($upcaster3, $upcaster1, $upcaster2);

        $canUpcast = $upcasterChain->canUpcast($event);
        $event = $upcasterChain->upcast($event);

        $this->assertTrue($canUpcast);
        $this->assertEquals($expectedVersion, $event->version());

        foreach ($expectedData as $key => $expectedValue) {
            $this->assertEquals($expectedValue, $event->payload()[$key]);
        }
    }

    /**
     * @dataProvider getInvalidUpcastData
     */
    public function testItShouldCanUpcastFalseSuccessfully(Event $event, Upcaster $upcaster): void
    {
        $result = $upcaster->canUpcast($event);

        $this->assertFalse($result);
    }

    public function getValidUpcastData(): array
    {
        return [
            [
                PersonPhoneChanged::create(UUID::generate(), null, '123456789'),
                [
                    'new_phone' => [
                        'language' => null,
                        'prefix' => null,
                        'value' => '123456789',
                    ],
                ],
                3
            ],
            [
                PersonEmailChanged::create(UUID::generate(), 'previous@email.com', 'new@email.com'),
                [
                    'new_email' => [
                        'validated' => false,
                        'value' => 'new@email.com',
                    ],
                ],
                2
            ],
        ];
    }

    public function getInvalidUpcastData(): array
    {
        return [
            [
                PersonEmailChanged::occur(UUID::generate()),
                new EventClassNameUpcasterChain(new PersonPhoneChangedFrom1To2Upcaster()),
            ],
            [
                PersonEmailChanged::occur(UUID::generate()),
                new EventClassNameUpcasterChain(),
            ],
        ];
    }
}
