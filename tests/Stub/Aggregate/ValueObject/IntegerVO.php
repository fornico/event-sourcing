<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Stub\Aggregate\ValueObject;

class IntegerVO
{
    private int $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    public function value(): int
    {
        return $this->value;
    }

    public function equals(IntegerVO $object): bool
    {
        return $this->value() === $object->value();
    }
}
