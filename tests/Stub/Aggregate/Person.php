<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Stub\Aggregate;

use Zisato\EventSourcing\Identity\Identity;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonAgeChanged;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonCreated;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonEmailChanged;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonNameChanged;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonPhoneChanged;
use Zisato\EventSourcing\Tests\Stub\Aggregate\ValueObject\IntegerVO;
use Zisato\EventSourcing\Tests\Stub\Aggregate\ValueObject\StringVO;

class Person extends Contact
{
    private StringVO $name;
    private ?IntegerVO $age;

    public static function create(
        Identity $aggregateId, 
        StringVO $name, 
        StringVO $email, 
        ?StringVO $phone, 
        ?IntegerVO $age
    ) {
        $instance = new static($aggregateId);

        $instance->recordAggregateEvent(
            PersonCreated::create(
                $aggregateId,
                $name->value(),
                $email->value(),
                $phone ? $phone->value() : null,
                $age ? $age->value() : null
            )
        );


        return $instance;
    }

    public function name(): StringVO
    {
        return $this->name;
    }

    public function age(): ?IntegerVO
    {
        return $this->age;
    }

    public function changeName(StringVO $newName): void
    {
        if (!$this->name->equals($newName)) {
            $this->recordAggregateEvent(
                PersonNameChanged::create(
                    $this->id(),
                    $this->name->value(),
                    $newName->value()
                )
            );
        }
    }

    public function changeEmail(StringVO $newEmail): void
    {
        if (!$this->email->equals($newEmail)) {
            $this->recordAggregateEvent(
                PersonEmailChanged::create(
                    $this->id(),
                    $this->email->value(),
                    $newEmail->value()
                )
            );
        }
    }

    public function changePhone(?StringVO $newPhone): void
    {
        $previousValue = $this->phone ? $this->phone->value() : null;
        $newValue = $newPhone ? $newPhone->value() : null;
        
        if ($previousValue !== $newValue) {
            $this->recordAggregateEvent(
                PersonPhoneChanged::create(
                    $this->id(),
                    $previousValue,
                    $newValue
                )
            );
        }
    }

    public function changeAge(?IntegerVO $newAge): void
    {
        $previousValue = $this->age ? $this->age->value() : null;
        $newValue = $newAge ? $newAge->value() : null;
        
        if ($previousValue !== $newValue) {
            $this->recordAggregateEvent(
                PersonAgeChanged::create(
                    $this->id(),
                    $previousValue,
                    $newValue
                )
            );
        }
    }

    public function applyPersonCreated(PersonCreated $event): void
    {
        $this->name = new StringVO($event->name());
        $this->email = new StringVO($event->email());
        $this->phone = $event->phone() ? new StringVO($event->phone()) : null;
        $this->age = $event->age() ? new IntegerVO($event->age()) : null;
    }

    public function applyPersonNameChanged(PersonNameChanged $event): void
    {
        $this->name = new StringVO($event->name());
    }

    public function applyPersonEmailChanged(PersonEmailChanged $event): void
    {
        $this->email = new StringVO($event->email());
    }

    public function applyPersonPhoneChanged(PersonPhoneChanged $event): void
    {
        $this->phone = $event->phone() ? new StringVO($event->phone()) : null;
    }

    public function applyPersonAgeChanged(PersonAgeChanged $event): void
    {
        $this->phone = $event->age() ? new IntegerVO($event->age()) : null;
    }
}
