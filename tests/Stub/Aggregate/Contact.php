<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Stub\Aggregate;

use Zisato\EventSourcing\Aggregate\AbstractAggregateRoot;
use Zisato\EventSourcing\Tests\Stub\Aggregate\ValueObject\StringVO;

abstract class Contact extends AbstractAggregateRoot
{
    protected StringVO $email;
    protected ?StringVO $phone;

    public abstract function changeEmail(StringVO $newEmail): void;
    public abstract function changePhone(?StringVO $newPhone): void;
}
