<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Stub\Aggregate\Event;

use Zisato\EventSourcing\Aggregate\Event\AbstractEvent;
use Zisato\EventSourcing\Identity\Identity;

class PersonAgeChanged extends AbstractEvent
{
    public const DEFAULT_VERSION = 1;

    public static function create(Identity $aggregateId, ?int $previousAge, ?int $newAge): PersonPhoneChanged
    {
        return static::occur(
            $aggregateId, 
            [
                'previous_age' => $previousAge,
                'new_age' => $newAge,
            ]
        );
    }

    public function age(): ?int
    {
        return $this->payload()['new_age'];
    }

    protected static function defaultVersion(): int
    {
        return static::DEFAULT_VERSION;
    }
}
