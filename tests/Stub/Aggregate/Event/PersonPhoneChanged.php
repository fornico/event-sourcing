<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Stub\Aggregate\Event;

use Zisato\EventSourcing\Aggregate\Event\AbstractEvent;
use Zisato\EventSourcing\Identity\Identity;

class PersonPhoneChanged extends AbstractEvent
{
    public const DEFAULT_VERSION = 1;

    public static function create(Identity $aggregateId, ?string $previousPhone, ?string $newPhone): PersonPhoneChanged
    {
        return static::occur(
            $aggregateId, 
            [
                'previous_phone' => $previousPhone,
                'new_phone' => $newPhone,
            ]
        );
    }

    public function phone(): ?string
    {
        return $this->payload()['new_phone'];
    }

    protected static function defaultVersion(): int
    {
        return static::DEFAULT_VERSION;
    }
}
