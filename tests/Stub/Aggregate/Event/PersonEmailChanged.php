<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Stub\Aggregate\Event;

use Zisato\EventSourcing\Aggregate\Event\AbstractEvent;
use Zisato\EventSourcing\Identity\Identity;

class PersonEmailChanged extends AbstractEvent
{
    public const DEFAULT_VERSION = 1;

    public static function create(Identity $aggregateId, string $previousEmail, string $newEmail): PersonEmailChanged
    {
        return static::occur(
            $aggregateId, 
            [
                'previous_email' => $previousEmail,
                'new_email' => $newEmail,
            ]
        );
    }

    public function email(): string
    {
        return $this->payload()['new_email'];
    }

    protected static function defaultVersion(): int
    {
        return static::DEFAULT_VERSION;
    }
}
