<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Stub\Aggregate\Event;

use Zisato\EventSourcing\Aggregate\Event\AbstractEvent;
use Zisato\EventSourcing\Identity\Identity;

class PersonCreated extends AbstractEvent
{
    public const DEFAULT_VERSION = 1;

    public static function create(
        Identity $aggregateId,
        string $name,
        string $email,
        ?string $phone,
        ?int $age
    ): PersonCreated {
        return static::occur(
            $aggregateId, 
            [
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'age' => $age,
            ]
        );
    }

    public function name(): string
    {
        return $this->payload()['name'];
    }

    public function email(): string
    {
        return $this->payload()['email'];
    }
    
    public function phone(): ?string
    {
        return $this->payload()['phone'];
    }
    
    public function age(): ?int
    {
        return $this->payload()['age'];
    }

    protected static function defaultVersion(): int
    {
        return static::DEFAULT_VERSION;
    }
}
