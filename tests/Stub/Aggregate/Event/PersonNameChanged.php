<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Stub\Aggregate\Event;

use Zisato\EventSourcing\Aggregate\Event\AbstractEvent;
use Zisato\EventSourcing\Identity\Identity;

class PersonNameChanged extends AbstractEvent
{
    public const DEFAULT_VERSION = 1;

    public static function create(Identity $aggregateId, string $previousName, string $newName): PersonNameChanged
    {
        return static::occur(
            $aggregateId, 
            [
                'previous_name' => $previousName,
                'new_name' => $newName,
            ]
        );
    }

    public function name(): string
    {
        return $this->payload()['new_name'];
    }

    protected static function defaultVersion(): int
    {
        return static::DEFAULT_VERSION;
    }
}
