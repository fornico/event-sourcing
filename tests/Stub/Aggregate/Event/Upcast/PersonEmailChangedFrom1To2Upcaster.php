<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Stub\Aggregate\Event\Upcast;

use Zisato\EventSourcing\Aggregate\Event\Event;
use Zisato\EventSourcing\Aggregate\Event\Upcast\AbstractEventUpcaster;
use Zisato\EventSourcing\Aggregate\Event\Upcast\EventClassNameUpcaster;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonEmailChanged;

class PersonEmailChangedFrom1To2Upcaster extends AbstractEventUpcaster implements EventClassNameUpcaster
{
    const VERSION_FROM = 1;
    const VERSION_TO = 2;

    public function eventClassName(): string
    {
        return PersonEmailChanged::class;
    }

    public function versionFrom(): int
    {
        return self::VERSION_FROM;
    }

    public function versionTo(): int
    {
        return self::VERSION_TO;
    }
    
    public function newPayload(array $payload): array
    {
        $payload['new_email'] = [
            'validated' => false,
            'value' => $payload['new_email'],
        ];

        return $payload;
    }

    public function upcast(Event $event): PersonEmailChanged
    {
        return parent::upcast($event);
    }
}
