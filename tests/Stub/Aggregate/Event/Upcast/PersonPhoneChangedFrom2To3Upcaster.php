<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Tests\Stub\Aggregate\Event\Upcast;

use Zisato\EventSourcing\Aggregate\Event\Event;
use Zisato\EventSourcing\Aggregate\Event\Upcast\AbstractEventUpcaster;
use Zisato\EventSourcing\Aggregate\Event\Upcast\EventClassNameUpcaster;
use Zisato\EventSourcing\Tests\Stub\Aggregate\Event\PersonPhoneChanged;

class PersonPhoneChangedFrom2To3Upcaster extends AbstractEventUpcaster implements EventClassNameUpcaster
{
    const VERSION_FROM = 2;
    const VERSION_TO = 3;

    public function eventClassName(): string
    {
        return PersonPhoneChanged::class;
    }

    public function versionFrom(): int
    {
        return self::VERSION_FROM;
    }

    public function versionTo(): int
    {
        return self::VERSION_TO;
    }
    
    public function newPayload(array $payload): array
    {
        $payload['new_phone']['language'] = null;

        return $payload;
    }

    public function upcast(Event $event): PersonPhoneChanged
    {
        return parent::upcast($event);
    }
}
