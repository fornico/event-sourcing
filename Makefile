php = php-7.4

.PHONY: docker-pull docker-test-pull

# docker
docker-pull:
	@docker-compose -f docker/$(php)/docker-compose.yml pull

docker-test-pull:
	@docker-compose -f tests/docker/$(php)/docker-compose.yml pull

# composer
composer-install: docker-pull
	@docker-compose -f docker/$(php)/docker-compose.yml run --rm php-cli composer install

composer-update: docker-pull
	@docker-compose -f docker/$(php)/docker-compose.yml run --rm php-cli composer update $(args)

# tests
test: docker-test-pull
	@docker-compose -f tests/docker/$(php)/docker-compose.yml run --rm php-cli tests/run.sh $(args)

test-coverage: docker-test-pull
	@docker-compose -f tests/docker/$(php)/docker-compose.yml run --rm php-cli-pcov tests/run.sh all-coverage $(args)

test-xdebug: docker-test-pull
	@docker-compose -f tests/docker/$(php)/docker-compose.yml run --rm php-cli-xdebug tests/run.sh $(args)

# utils
infection: docker-pull
	@docker-compose -f docker/$(php)/docker-compose.yml run --rm php-cli-pcov bin/infection

phpcbf: docker-pull
	@docker-compose -f docker/$(php)/docker-compose.yml run --rm php-cli bin/phpcbf --standard=PSR12 src/

phpstan: docker-pull
	@docker-compose -f docker/$(php)/docker-compose.yml run --rm php-cli bin/phpstan analyse -l 7 src/

psalm: docker-pull
	@docker-compose -f docker/$(php)/docker-compose.yml run --rm php-cli bin/psalm