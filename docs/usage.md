### Aggregate root

Aggregate root should implements `Zisato\EventSourcing\Aggregate\AggregateRoot` or your can extends from class `Zisato\EventSourcing\Aggregate\AbstractAggregateRoot`. Constructor is final, forcing to create static named constructors

```php
use Zisato\EventSourcing\Aggregate\AbstractAggregateRoot;
use Zisato\EventSourcing\Identity\Identity;

class MyAggregate extends AbstractAggregateRoot
{
    private string $myProperty;

    public static function create(Identity $id, string $myProperty): AggregateRoot
    {
        $instance = new static($id);

        $instance->recordThat(
            MyAggregateCreated::occur(
                $id,
                [
                    'my_property' => $myProperty,
                ]
            )
            // or
            MyAggregateCreated::create($id, $myProperty)
        );

        return $instance;
    }

    public function changeMyProperty(string $newMyProperty): void
    {
        if ($this->myProperty !== $newMyProperty) {
            $this->recordThat(
                MyAggregateEvent::occur(
                    $this->id(),
                    [
                        'previous_my_property' => $this->myProperty,
                        'new_my_property' => $newMyProperty,
                    ]
                )
                // or
                MyAggregateEvent::create(
                    $this->id(),
                    $this->myProperty,
                    $newMyProperty
                )
            );
        }
    }

    protected function applyMyAggregateCreated(MyAggregateCreated $event): void
    {
        $this->myProperty = $event->getData()['my_property'];
        // or
        $this->myProperty = $event->myProperty();
    }

    protected function applyMyAggregateEvent(MyAggregateEvent $event): void
    {
        $this->myProperty = $event->getData()['new_my_property'];
        // or
        $this->myProperty = $event->myProperty();
    }
}
```

### Aggregate event

Aggregate events should implements `Zisato\EventSourcing\Aggregate\Event\Event` or you can extends from class `Zisato\EventSourcing\Aggregate\Event\AbstractEvent`. Constructor is final, forcing to create static named constructors and taking advantage of payload type hinting, although you could use constructor to instance your aggregate event, it is recommended to use `occur` method.

Also you will need to implement the following methods:

`protected static function defaultVersion(): int` The default argument value of aggregate event version for `occur` method. More detail in [Upcast](./upcast.md) section


```php
use Zisato\EventSourcing\Aggregate\Event\AbstractEvent;
use Zisato\EventSourcing\Aggregate\Event\Event;

class MyAggregateCreated extends AbstractEvent
{
    private const DEFAULT_VERSION = 1;

    protected static function defaultVersion(): int
    {
        return static::DEFAULT_VERSION;
    }

    public static function create(UUID $aggregateId, string $myProperty): Event
    {
        return static::occur(
            $aggregateId,
            [
                'my_property' => $myProperty,
            ]
        );
    }

    public function myProperty(): string
    {
        return $this->data()['my_property'];
    }
}
```