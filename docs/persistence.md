
### Aggregate root repository
Aggregate root repository should implements `Zisato\EventSourcing\Aggregate\Repository\AggregateRootRepository` or your can use the class `Zisato\EventSourcing\Aggregate\Repository\GenericAggregateRootRepository`. It has the following constructor dependencies:

- `aggregate root class name` *Required
- `Zisato\EventSourcing\Aggregate\Event\Store\EventStore` *Required
- `Zisato\EventSourcing\Aggregate\Event\Decorator\EventDecorator` Nullable
- `Zisato\EventSourcing\Aggregate\Event\Bus\EventBus` Nullable

```php
<?php

use Zisato\EventSourcing\Aggregate\Repository\GenericAggregateRootRepository;

...

$repository = new GenericAggregateRootRepository(,
    MyAggregate::class,
    $eventStore
);

$repository = new GenericAggregateRootRepository(,
    MyAggregate::class,
    $eventStore,
    $eventDecorator,
    $eventCollector
);
```

The `save` method will decorate the aggregate events before persist them into event store and send to an Event Bus to be dispatched if needed.

For EventStore you can use the current DBAL event store implementation from `zisato/event-sourcing-event-store-dbal` or create your own implementing `Zisato\EventSourcing\Aggregate\Event\Store\EventStore`

For EventDecorator you can use the provided event decorator `Zisato\EventSourcing\Aggregate\Event\Decorator\GenericEventDecorator` or create your own implementing `Zisato\EventSourcing\Aggregate\Event\Decorator\EventDecorator`

For EventCollector you can use the provided event collector `Zisato\EventSourcing\Aggregate\Event\Bus\InMemoryEventCollector` or create your own implementing `Zisato\EventSourcing\Aggregate\Event\Collector\EventCollector`