### Upcast
Upcasting is provided at event serialization stage with the serializer `Zisato\EventSourcing\Aggregate\Event\Serializer\UpcasterEventSerializer` 
dependencies:

- `Zisato\EventSourcing\Aggregate\Event\Upcast\Upcaster`

The upcaster class will need to implement the following methods:
- `public function canUpcast(Event $event): bool` This will decide if event argument can be upcasted by this class
- `public function upcast(Event $event): Event` This will return the new upcasted event
```php

use Zisato\EventSourcing\Aggregate\Event\Upcast\Upcaster;

class EventFrom1To2Upcaster implements Upcaster
{
    const VERSION_FROM = 1;
    const VERSION_TO = 2;

    public function canUpcast(Event $event): bool
    {
        return $event->version() === self::VERSION_FROM;
    }

    public function upcast(Event $event): Event
    {
        $newPayload = $event->payload();
        $newPayload['upcasted_1_to_2_key'] = 'upcasted_1_to_2_value';

        return Event::reconstitute(
            $event->aggregateId(),
            $event->aggregateVersion(),
            $event->createdAt(),
            $newPayload,
            self::VERSION_TO,
            $event->metadata()
        );
    }
}

```

Multiple upcasters grouped by event can be done using `Zisato\EventSourcing\Aggregate\Event\Upcast\EventClassNameUpcasterChain` which implements `Upcaster` and requires one or more of `Zisato\EventSourcing\Aggregate\Event\Upcast\EventClassNameUpcaster`, this also implements `Upcaster` and adds a new method `public function eventClassName(): string` to return the event class name 
```php

use Zisato\EventSourcing\Aggregate\Aggregate\Event\Upcast\EventClassNameUpcasterChain;
use Zisato\EventSourcing\Aggregate\Event\Serializer\UpcasterEventSerializer;
use Zisato\EventSourcing\Aggregate\Event\Upcast\EventClassNameUpcaster;

class EventFrom1To2Upcaster implements EventClassNameUpcaster
{
    ...
    
    public function eventClassName(): string
    {
        return Event::class;
    }

    ...
}

class EventFrom2To3Upcaster implements EventClassNameUpcaster
{
    ...
    
    public function eventClassName(): string
    {
        return Event::class;
    }

    ...
}

class Event2From1To3Upcaster implements EventClassNameUpcaster
{
    ...
    
    public function eventClassName(): string
    {
        return Event2::class;
    }

    ...
}


$upcaster1 = new EventFrom1To2Upcaster();
$upcaster2 = new EventFrom2To3Upcaster();
$upcaster3 = new Event2From1To3Upcaster();
$upcasterChain = new EventClassNameUpcasterChain($upcaster1, $upcaster2, $upcaster3);

$serializer = new UpcasterEventSerializer($eventSerializer, $upcasterChain);
```