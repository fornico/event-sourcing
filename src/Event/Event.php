<?php

namespace Zisato\EventSourcing\Event;

interface Event
{
    public function createdAt(): \DateTimeImmutable;

    /**
     * @return array<string, mixed>
     */
    public function payload(): array;
}
