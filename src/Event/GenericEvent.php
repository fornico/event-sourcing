<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Event;

class GenericEvent implements Event
{
    private \DateTimeImmutable $createdAt;
    /**
     * @var array<string, mixed> $payload
     */
    private array $payload;

    /**
     * @param \DateTimeImmutable $createdAt
     * @param array<string, mixed> $payload
     */
    public function __construct(\DateTimeImmutable $createdAt, array $payload)
    {
        $this->createdAt = $createdAt;
        $this->payload = $payload;
    }

    public function createdAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function payload(): array
    {
        return $this->payload;
    }
}
