<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\ValueObject;

class Version
{
    private const VALUE_MIN = 0;

    private int $value;

    final private function __construct(int $value)
    {
        $this->checkValidValue($value);

        $this->value = $value;
    }

    public static function create(int $value): Version
    {
        return new static($value);
    }

    public static function zero(): Version
    {
        return new static(static::VALUE_MIN);
    }

    public function value(): int
    {
        return $this->value;
    }

    public function next(): Version
    {
        return new static($this->value + 1);
    }

    public function equals(Version $version): bool
    {
        return $this->value() === $version->value();
    }

    private function checkValidValue(int $value): void
    {
        if ($value < static::VALUE_MIN) {
            throw new \InvalidArgumentException(\sprintf(
                'Invalid Version value. Min allowed: %d',
                static::VALUE_MIN
            ));
        }
    }
}
