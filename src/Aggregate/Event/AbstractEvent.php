<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event;

use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Event\GenericEvent;
use Zisato\EventSourcing\Identity\Identity;

abstract class AbstractEvent extends GenericEvent implements Event
{
    private Identity $aggregateId;
    private Version $aggregateVersion;
    private int $version;
    /**
     * @var array<string, mixed> $metadata
     */
    private array $metadata;

    /**
     * @param Identity $aggregateId
     * @param Version $aggregateVersion
     * @param \DateTimeImmutable $createdAt
     * @param array<string, mixed> $payload
     * @param int $version
     * @param array<string, mixed> $metadata
     */
    final protected function __construct(
        Identity $aggregateId,
        Version $aggregateVersion,
        \DateTimeImmutable $createdAt,
        array $payload,
        int $version,
        array $metadata
    ) {
        parent::__construct($createdAt, $payload);

        $this->aggregateId = $aggregateId;
        $this->aggregateVersion = $aggregateVersion;
        $this->version = $version;
        $this->metadata = $metadata;
    }

    abstract protected static function defaultVersion(): int;

    public static function occur(
        Identity $aggregateId,
        array $payload = []
    ): Event {
        return new static(
            $aggregateId,
            Version::zero(),
            new \DateTimeImmutable(),
            $payload,
            static::defaultVersion(),
            []
        );
    }

    public static function reconstitute(
        Identity $aggregateId,
        Version $aggregateVersion,
        \DateTimeImmutable $createdAt,
        array $payload,
        int $version,
        array $metadata
    ): Event {
        return new static(
            $aggregateId,
            $aggregateVersion,
            $createdAt,
            $payload,
            $version,
            $metadata
        );
    }

    public function addMetadata(string $key, $value): Event
    {
        $newMetadata = $this->metadata;

        $newMetadata[$key] = $value;

        return new static(
            $this->aggregateId(),
            $this->aggregateVersion(),
            $this->createdAt(),
            $this->payload(),
            $this->version(),
            $newMetadata
        );
    }

    public function withAggregateVersion(Version $aggregateVersion): Event
    {
        return new static(
            $this->aggregateId(),
            $aggregateVersion,
            $this->createdAt(),
            $this->payload(),
            $this->version(),
            $this->metadata(),
        );
    }

    public function aggregateId(): Identity
    {
        return $this->aggregateId;
    }

    public function aggregateVersion(): Version
    {
        return $this->aggregateVersion;
    }

    public function version(): int
    {
        return $this->version;
    }

    public function metadata(): array
    {
        return $this->metadata;
    }
}
