<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event\Stream;

use Zisato\EventSourcing\Aggregate\Event\Event;

class GeneratorEventStream implements EventStream
{
    private const EMPTY_COUNT = 0;

    /**
     * @var array<Event> $events
     */
    private array $events;
    private int $count;

    final private function __construct()
    {
        $this->events = [];
        $this->count = self::EMPTY_COUNT;
    }

    public static function create(): EventStream
    {
        return new self();
    }

    public function add(Event $event): void
    {
        $this->events[] = $event;

        $this->count++;
    }

    public function count(): int
    {
        return $this->count;
    }

    /**
     * @return iterable<Event>
     */
    public function events(): iterable
    {
        yield from $this->events;
    }

    public function isEmpty(): bool
    {
        return $this->count === self::EMPTY_COUNT;
    }
}
