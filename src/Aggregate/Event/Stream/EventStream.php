<?php

namespace Zisato\EventSourcing\Aggregate\Event\Stream;

use Zisato\EventSourcing\Aggregate\Event\Event;

interface EventStream extends \Countable
{
    public function add(Event $event): void;

    /**
     * @return iterable<Event>
     */
    public function events(): iterable;

    public function isEmpty(): bool;
}
