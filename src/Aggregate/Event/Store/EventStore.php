<?php

namespace Zisato\EventSourcing\Aggregate\Event\Store;

use Zisato\EventSourcing\Aggregate\Event\Event;
use Zisato\EventSourcing\Aggregate\Event\Stream\EventStream;
use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Identity\Identity;

interface EventStore
{
    public function exists(Identity $aggregateId): bool;

    public function get(Identity $aggregateId, Version $fromAggregateVersion): EventStream;

    public function save(Event $event): void;
}
