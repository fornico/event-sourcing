<?php

namespace Zisato\EventSourcing\Aggregate\Event;

use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Event\Event as BaseEvent;
use Zisato\EventSourcing\Identity\Identity;

interface Event extends BaseEvent
{
    /**
     * @param Identity $aggregateId
     * @param array<string, mixed> $payload
     *
     * @return Event
     */
    public static function occur(Identity $aggregateId, array $payload): Event;

    /**
     * @param Identity $aggregateId
     * @param Version $aggregateVersion
     * @param \DateTimeImmutable $createdAt
     * @param array<string, mixed> $payload
     * @param int $version
     * @param array<string, mixed> $metadata
     *
     * @return Event
     */
    public static function reconstitute(
        Identity $aggregateId,
        Version $aggregateVersion,
        \DateTimeImmutable $createdAt,
        array $payload,
        int $version,
        array $metadata
    ): Event;

    /**
     * @param string $key
     * @param mixed $value
     *
     * @return Event
     */
    public function addMetadata(string $key, $value): Event;

    public function withAggregateVersion(Version $aggregateVersion): Event;

    public function aggregateId(): Identity;

    public function aggregateVersion(): Version;

    public function version(): int;

    /**
     * @return array<string, mixed>
     */
    public function metadata(): array;
}
