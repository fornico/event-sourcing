<?php

namespace Zisato\EventSourcing\Aggregate\Event\Bus;

use Zisato\EventSourcing\Aggregate\Event\Event;

interface EventBus
{
    public function handle(Event $event): void;
}
