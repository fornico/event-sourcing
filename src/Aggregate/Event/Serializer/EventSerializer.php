<?php

namespace Zisato\EventSourcing\Aggregate\Event\Serializer;

use Zisato\EventSourcing\Aggregate\Event\Event;

interface EventSerializer
{
    public const DATE_FORMAT = 'Y-m-d H:i:s.u';

    /**
     * @param array<string, mixed> $data
     *
     * @return Event
     */
    public function fromArray(array $data): Event;

    /**
     * @param Event $event
     *
     * @return array<string, mixed>
     */
    public function toArray(Event $event): array;
}
