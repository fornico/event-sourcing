<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event\Serializer;

use Zisato\EventSourcing\Aggregate\Event\Event;
use Zisato\EventSourcing\Aggregate\Event\Upcast\Upcaster;

class UpcasterEventSerializer implements EventSerializer
{
    private EventSerializer $eventSerializer;
    private Upcaster $upcaster;

    public function __construct(
        EventSerializer $eventSerializer,
        Upcaster $upcaster
    ) {
        $this->eventSerializer = $eventSerializer;
        $this->upcaster = $upcaster;
    }

    public function fromArray(array $data): Event
    {
        $event = $this->eventSerializer->fromArray($data);

        if ($this->upcaster->canUpcast($event)) {
            $event = $this->upcaster->upcast($event);
        }

        return $event;
    }

    public function toArray(Event $event): array
    {
        return $this->eventSerializer->toArray($event);
    }
}
