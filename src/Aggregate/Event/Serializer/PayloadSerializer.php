<?php

namespace Zisato\EventSourcing\Aggregate\Event\Serializer;

interface PayloadSerializer
{
    /**
     * @param string $payload
     *
     * @return array<string, mixed>
     */
    public function deserialize(string $payload): array;

    /**
     * @param array<string, mixed> $payload
     *
     * @return string
     */
    public function serialize(array $payload): string;
}
