<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event\Serializer;

class JSONPayloadSerializer implements PayloadSerializer
{
    private const JSON_ASSOC = true;
    private const JSON_DEPTH = 512;
    private const JSON_DECODE_OPTIONS = \JSON_THROW_ON_ERROR;
    private const JSON_ENCODE_OPTIONS = \JSON_UNESCAPED_UNICODE |
        \JSON_THROW_ON_ERROR;

    public function deserialize(string $payload): array
    {
        return \json_decode(
            $payload,
            self::JSON_ASSOC,
            self::JSON_DEPTH,
            self::JSON_DECODE_OPTIONS
        );
    }

    public function serialize(array $payload): string
    {
        return \json_encode(
            $payload,
            self::JSON_ENCODE_OPTIONS
        );
    }
}
