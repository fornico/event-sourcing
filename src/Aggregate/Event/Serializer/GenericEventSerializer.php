<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event\Serializer;

use Zisato\EventSourcing\Aggregate\Event\Event;
use Zisato\EventSourcing\Aggregate\Event\Serializer\MetadataSerializer;
use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Identity\Factory\IdentityFactory;

class GenericEventSerializer implements EventSerializer
{
    private IdentityFactory $identityFactory;
    private PayloadSerializer $payloadSerializer;
    private MetadataSerializer $metadataSerializer;

    public function __construct(
        IdentityFactory $identityFactory,
        PayloadSerializer $payloadSerializer,
        MetadataSerializer $metadataSerializer
    ) {
        $this->identityFactory = $identityFactory;
        $this->payloadSerializer = $payloadSerializer;
        $this->metadataSerializer = $metadataSerializer;
    }

    public function fromArray(array $data): Event
    {
        /** @var callable $callable */
        $callable = [$data['event_class'], 'reconstitute'];

        return \call_user_func(
            $callable,
            $this->identityFactory->fromString($data['aggregate_id']),
            Version::create($data['aggregate_version']),
            new \DateTimeImmutable($data['created_at']),
            $this->payloadSerializer->deserialize($data['payload']),
            $data['version'],
            $this->metadataSerializer->deserialize($data['metadata'])
        );
    }

    public function toArray(Event $event): array
    {
        return [
            'event_class' => \get_class($event),
            'aggregate_id' => $event->aggregateId()->value(),
            'aggregate_version' => $event->aggregateVersion()->value(),
            'created_at' => $event->createdAt()->format(static::DATE_FORMAT),
            'payload' => $this->payloadSerializer->serialize($event->payload()),
            'version' => $event->version(),
            'metadata' => $this->metadataSerializer->serialize($event->metadata()),
        ];
    }
}
