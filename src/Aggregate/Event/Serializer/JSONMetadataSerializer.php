<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event\Serializer;

class JSONMetadataSerializer implements MetadataSerializer
{
    private const JSON_ASSOC = true;
    private const JSON_DEPTH = 512;
    private const JSON_DECODE_OPTIONS = \JSON_THROW_ON_ERROR;
    private const JSON_ENCODE_OPTIONS = \JSON_UNESCAPED_UNICODE |
        \JSON_THROW_ON_ERROR;

    public function deserialize(string $metadata): array
    {
        return \json_decode(
            $metadata,
            self::JSON_ASSOC,
            self::JSON_DEPTH,
            self::JSON_DECODE_OPTIONS
        );
    }

    public function serialize(array $metadata): string
    {
        return \json_encode(
            $metadata,
            self::JSON_ENCODE_OPTIONS
        );
    }
}
