<?php

namespace Zisato\EventSourcing\Aggregate\Event\Serializer;

interface MetadataSerializer
{
    /**
     * @param string $metadata
     *
     * @return array<string, mixed>
     */
    public function deserialize(string $metadata): array;

    /**
     * @param array<string, mixed> $metadata
     *
     * @return string
     */
    public function serialize(array $metadata): string;
}
