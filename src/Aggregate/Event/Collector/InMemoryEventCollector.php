<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event\Collector;

use Zisato\EventSourcing\Aggregate\Event\Event;

class InMemoryEventCollector implements EventCollector
{
    /**
     * @var array<Event> $events
     */
    private array $events;

    public function __construct()
    {
        $this->events = [];
    }

    public function add(Event $event): void
    {
        $this->events[] = $event;
    }

    public function release(): iterable
    {
        $events = $this->events;

        $this->events = [];

        yield from $events;
    }
}
