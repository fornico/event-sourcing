<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event\Collector;

use Zisato\EventSourcing\Aggregate\Event\Event;

class NullEventCollector implements EventCollector
{
    public function add(Event $event): void
    {
        // do nothing with event
    }

    public function release(): iterable
    {
        return [];
    }
}
