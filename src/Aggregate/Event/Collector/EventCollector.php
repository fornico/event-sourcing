<?php

namespace Zisato\EventSourcing\Aggregate\Event\Collector;

use Zisato\EventSourcing\Aggregate\Event\Event;

interface EventCollector
{
    public function add(Event $event): void;

    /**
     * @return iterable<Event>
     */
    public function release(): iterable;
}
