<?php

namespace Zisato\EventSourcing\Aggregate\Event\Dispatcher;

use Zisato\EventSourcing\Aggregate\Event\Event;

interface EventDispatcher
{
    public function dispatch(Event $event): void;
}
