<?php

namespace Zisato\EventSourcing\Aggregate\Event\Decorator;

use Zisato\EventSourcing\Aggregate\Event\Event;

interface EventDecorator
{
    public function decorate(Event $event): Event;
}
