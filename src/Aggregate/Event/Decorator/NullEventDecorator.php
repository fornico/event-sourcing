<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event\Decorator;

use Zisato\EventSourcing\Aggregate\Event\Event;

class NullEventDecorator implements EventDecorator
{
    public function decorate(Event $event): Event
    {
        return $event;
    }
}
