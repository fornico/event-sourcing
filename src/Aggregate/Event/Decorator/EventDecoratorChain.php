<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event\Decorator;

use Zisato\EventSourcing\Aggregate\Event\Event;

class EventDecoratorChain implements EventDecorator
{
    /**
     * @var array<EventDecorator> $decorators
     */
    private array $decorators;

    public function __construct(EventDecorator ...$decorators)
    {
        $this->decorators = $decorators;
    }

    public function decorate(Event $event): Event
    {
        foreach ($this->decorators as $decorator) {
            $event = $decorator->decorate($event);
        }

        return $event;
    }
}
