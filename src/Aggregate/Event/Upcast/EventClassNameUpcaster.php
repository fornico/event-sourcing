<?php

namespace Zisato\EventSourcing\Aggregate\Event\Upcast;

interface EventClassNameUpcaster extends Upcaster
{
    public function eventClassName(): string;
}
