<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event\Upcast;

use Zisato\EventSourcing\Aggregate\Event\Event;

class EventClassNameUpcasterChain implements Upcaster
{
    /**
     * @var array<string, array<EventClassNameUpcaster>> $upcasters
     */
    private array $upcasters = [];

    public function __construct(EventClassNameUpcaster ...$upcasters)
    {
        foreach ($upcasters as $upcaster) {
            $this->upcasters[$upcaster->eventClassName()][] = $upcaster;
        }
    }

    public function canUpcast(Event $event): bool
    {
        return isset($this->upcasters[\get_class($event)]);
    }

    public function upcast(Event $event): Event
    {
        $upcasters = $this->upcasters[\get_class($event)] ?? [];

        foreach ($upcasters as $upcaster) {
            if ($upcaster->canUpcast($event)) {
                $event = $upcaster->upcast($event);
            }
        }

        return $event;
    }
}
