<?php

namespace Zisato\EventSourcing\Aggregate\Event\Upcast;

use Zisato\EventSourcing\Aggregate\Event\Event;

abstract class AbstractEventUpcaster implements Upcaster
{
    abstract public function versionFrom(): int;

    abstract public function versionTo(): int;

    abstract public function newPayload(array $payload): array;

    public function canUpcast(Event $event): bool
    {
        return $event->version() === $this->versionFrom();
    }

    public function upcast(Event $event): Event
    {
        $newPayload = $this->newPayload($event->payload());

        return $event::reconstitute(
            $event->aggregateId(),
            $event->aggregateVersion(),
            $event->createdAt(),
            $newPayload,
            $this->versionTo(),
            $event->metadata()
        );
    }
}
