<?php

namespace Zisato\EventSourcing\Aggregate\Event\Upcast;

use Zisato\EventSourcing\Aggregate\Event\Event;

interface Upcaster
{
    public function canUpcast(Event $event): bool;

    public function upcast(Event $event): Event;
}
