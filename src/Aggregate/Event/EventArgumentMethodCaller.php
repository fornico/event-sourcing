<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Event;

use Zisato\EventSourcing\Aggregate\Exception\MethodNotDefinedException;

trait EventArgumentMethodCaller
{
    public function callMethod(Event $event, string $prefix): void
    {
        $className = \explode('\\', \get_class($event));
        $shortName = \array_pop($className);
        $method = $prefix . $shortName;

        if (!\method_exists($this, $method)) {
            throw new MethodNotDefinedException(\sprintf(
                'Method %s(%s $event) not defined in class %s',
                $method,
                $shortName,
                static::class
            ));
        }

        $this->{$method}($event);
    }
}
