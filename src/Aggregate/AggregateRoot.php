<?php

namespace Zisato\EventSourcing\Aggregate;

use Zisato\EventSourcing\Aggregate\Event\Stream\EventStream;
use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Identity\Identity;

interface AggregateRoot
{
    public static function reconstitute(Identity $id, EventStream $eventStream): AggregateRoot;

    public function id(): Identity;

    public function version(): Version;

    public function replyEvents(EventStream $eventStream): void;

    public function releaseRecordedEvents(): EventStream;

    public function countRecordedEvents(): int;
}
