<?php

namespace Zisato\EventSourcing\Aggregate\Serializer;

use Zisato\EventSourcing\Aggregate\AggregateRoot;

interface AggregateRootSerializer
{
    public function serialize(AggregateRoot $aggregateRoot): string;

    public function deserialize(string $aggregateRoot): AggregateRoot;
}
