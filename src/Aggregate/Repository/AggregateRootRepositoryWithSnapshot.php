<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Repository;

use Zisato\EventSourcing\Aggregate\AggregateRoot;
use Zisato\EventSourcing\Aggregate\Event\Collector\EventCollector;
use Zisato\EventSourcing\Aggregate\Event\Decorator\EventDecorator;
use Zisato\EventSourcing\Aggregate\Event\Store\EventStore;
use Zisato\EventSourcing\Aggregate\Snapshot\Snapshotter;
use Zisato\EventSourcing\Identity\Identity;

class AggregateRootRepositoryWithSnapshot extends GenericAggregateRootRepository
{
    private Snapshotter $snapshotter;

    public function __construct(
        string $aggregateRootName,
        EventStore $eventStore,
        Snapshotter $snapshotter,
        ?EventDecorator $eventDecorator = null,
        ?EventCollector $eventCollector = null
    ) {
        parent::__construct($aggregateRootName, $eventStore, $eventDecorator, $eventCollector);

        $this->snapshotter = $snapshotter;
    }

    public function get(Identity $aggregateId): AggregateRoot
    {
        $aggregateRoot = $this->snapshotter->get($aggregateId);

        if ($aggregateRoot !== null) {
            $eventStream = $this->eventStore->get(
                $aggregateId,
                $aggregateRoot->version()
            );

            $aggregateRoot->replyEvents($eventStream);

            return $aggregateRoot;
        }

        return parent::get($aggregateId);
    }

    public function save(AggregateRoot $aggregateRoot): void
    {
        parent::save($aggregateRoot);

        $this->snapshotter->handle($aggregateRoot);
    }
}
