<?php

namespace Zisato\EventSourcing\Aggregate\Repository;

use Zisato\EventSourcing\Aggregate\AggregateRoot;
use Zisato\EventSourcing\Identity\Identity;

interface AggregateRootRepository
{
    public function get(Identity $aggregateId): AggregateRoot;

    public function save(AggregateRoot $aggregateRoot): void;
}
