<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Repository;

use Zisato\EventSourcing\Aggregate\AggregateRoot;
use Zisato\EventSourcing\Aggregate\Event\Collector\EventCollector;
use Zisato\EventSourcing\Aggregate\Event\Collector\NullEventCollector;
use Zisato\EventSourcing\Aggregate\Event\Decorator\EventDecorator;
use Zisato\EventSourcing\Aggregate\Event\Decorator\NullEventDecorator;
use Zisato\EventSourcing\Aggregate\Event\Store\EventStore;
use Zisato\EventSourcing\Aggregate\Exception\AggregateRootNotFoundException;
use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Identity\Identity;

class GenericAggregateRootRepository implements AggregateRootRepository
{
    protected string $aggregateRootName;
    protected EventStore $eventStore;
    protected EventDecorator $eventDecorator;
    protected EventCollector $eventCollector;

    public function __construct(
        string $aggregateRootName,
        EventStore $eventStore,
        ?EventDecorator $eventDecorator = null,
        ?EventCollector $eventCollector = null
    ) {
        $this->aggregateRootName = $aggregateRootName;
        $this->eventStore = $eventStore;
        $this->eventDecorator = $eventDecorator ?: new NullEventDecorator();
        $this->eventCollector = $eventCollector ?: new NullEventCollector();
    }

    public function get(Identity $aggregateId): AggregateRoot
    {
        $eventStream = $this->eventStore->get($aggregateId, Version::zero());

        if ($eventStream->isEmpty()) {
            throw new AggregateRootNotFoundException(\sprintf(
                'AggregateRoot with id %s not found',
                $aggregateId->value()
            ));
        }

        /** @var callable $callable */
        $callable = [$this->aggregateRootName, 'reconstitute'];

        return \call_user_func($callable, $aggregateId, $eventStream);
    }

    public function save(AggregateRoot $aggregateRoot): void
    {
        $events = $aggregateRoot->releaseRecordedEvents()->events();

        foreach ($events as $event) {
            $event = $this->eventDecorator->decorate($event);

            $this->eventStore->save($event);

            $this->eventCollector->add($event);
        }
    }
}
