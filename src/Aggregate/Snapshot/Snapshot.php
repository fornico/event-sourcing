<?php

namespace Zisato\EventSourcing\Aggregate\Snapshot;

use Zisato\EventSourcing\Aggregate\AggregateRoot;
use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Identity\Identity;

interface Snapshot
{
    public static function create(
        AggregateRoot $aggregateRoot,
        \DateTimeImmutable $createdAt
    ): Snapshot;

    public function aggregateRoot(): AggregateRoot;

    public function aggregateRootClassName(): string;

    public function aggregateRootId(): Identity;

    public function aggregateRootVersion(): Version;

    public function createdAt(): \DateTimeImmutable;
}
