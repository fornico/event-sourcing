<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Snapshot;

use Zisato\EventSourcing\Aggregate\AggregateRoot;
use Zisato\EventSourcing\Aggregate\Snapshot\Service\SnapshotService;
use Zisato\EventSourcing\Aggregate\Snapshot\Store\SnapshotStore;
use Zisato\EventSourcing\Aggregate\Snapshot\Strategy\SnapshotStrategy;
use Zisato\EventSourcing\Identity\Identity;

class GenericSnapshotter implements Snapshotter
{
    private SnapshotStore $snapshotStore;
    private SnapshotStrategy $snapshotStrategy;
    private SnapshotService $snapshotService;

    public function __construct(
        SnapshotStore $snapshotStore,
        SnapshotStrategy $snapshotStrategy,
        SnapshotService $snapshotService
    ) {
        $this->snapshotStore = $snapshotStore;
        $this->snapshotStrategy = $snapshotStrategy;
        $this->snapshotService = $snapshotService;
    }

    public function get(Identity $aggregateId): ?AggregateRoot
    {
        $snapshot = $this->snapshotStore->get($aggregateId);

        if ($snapshot === null) {
            return null;
        }

        return $snapshot->aggregateRoot();
    }

    public function handle(AggregateRoot $aggregateRoot): void
    {
        if ($this->snapshotStrategy->shouldCreateSnapshot($aggregateRoot)) {
            $this->snapshotService->create(
                GenericSnapshot::create($aggregateRoot, new \DateTimeImmutable())
            );
        }
    }
}
