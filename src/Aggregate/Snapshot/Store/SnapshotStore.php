<?php

namespace Zisato\EventSourcing\Aggregate\Snapshot\Store;

use Zisato\EventSourcing\Aggregate\Snapshot\Snapshot;
use Zisato\EventSourcing\Identity\Identity;

interface SnapshotStore
{
    public function get(Identity $aggregateId): ?Snapshot;

    public function save(Snapshot $snapshot): void;
}
