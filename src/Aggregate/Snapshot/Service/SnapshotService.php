<?php

namespace Zisato\EventSourcing\Aggregate\Snapshot\Service;

use Zisato\EventSourcing\Aggregate\Snapshot\Snapshot;

interface SnapshotService
{
    public function create(Snapshot $snapshot): void;
}
