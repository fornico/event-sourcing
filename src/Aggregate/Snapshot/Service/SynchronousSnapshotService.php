<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Snapshot\Service;

use Zisato\EventSourcing\Aggregate\Snapshot\Snapshot;
use Zisato\EventSourcing\Aggregate\Snapshot\Store\SnapshotStore;

class SynchronousSnapshotService implements SnapshotService
{
    private SnapshotStore $snapshotStore;

    public function __construct(SnapshotStore $snapshotStore)
    {
        $this->snapshotStore = $snapshotStore;
    }

    public function create(Snapshot $snapshot): void
    {
        $this->snapshotStore->save($snapshot);
    }
}
