<?php

namespace Zisato\EventSourcing\Aggregate\Snapshot\Strategy;

use Zisato\EventSourcing\Aggregate\AggregateRoot;

interface SnapshotStrategy
{
    public function shouldCreateSnapshot(AggregateRoot $aggregateRoot): bool;
}
