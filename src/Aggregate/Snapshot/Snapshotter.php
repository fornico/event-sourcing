<?php

namespace Zisato\EventSourcing\Aggregate\Snapshot;

use Zisato\EventSourcing\Aggregate\AggregateRoot;
use Zisato\EventSourcing\Identity\Identity;

interface Snapshotter
{
    public function get(Identity $aggregateId): ?AggregateRoot;

    public function handle(AggregateRoot $aggregateRoot): void;
}
