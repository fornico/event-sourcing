<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Snapshot;

use Zisato\EventSourcing\Aggregate\AggregateRoot;
use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Identity\Identity;

class GenericSnapshot implements Snapshot
{
    private AggregateRoot $aggregateRoot;
    private \DateTimeImmutable $createdAt;

    final protected function __construct(
        AggregateRoot $aggregateRoot,
        \DateTimeImmutable $createdAt
    ) {
        $this->aggregateRoot = $aggregateRoot;
        $this->createdAt = $createdAt;
    }

    public static function create(
        AggregateRoot $aggregateRoot,
        \DateTimeImmutable $createdAt
    ): Snapshot {
        return new static($aggregateRoot, $createdAt);
    }

    public function aggregateRoot(): AggregateRoot
    {
        return $this->aggregateRoot;
    }

    public function aggregateRootClassName(): string
    {
        return \get_class($this->aggregateRoot);
    }

    public function aggregateRootId(): Identity
    {
        return $this->aggregateRoot->id();
    }

    public function aggregateRootVersion(): Version
    {
        return $this->aggregateRoot->version();
    }

    public function createdAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }
}
