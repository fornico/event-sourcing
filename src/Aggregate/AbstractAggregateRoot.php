<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate;

use Zisato\EventSourcing\Aggregate\Event\Event;
use Zisato\EventSourcing\Aggregate\Event\EventArgumentMethodCaller;
use Zisato\EventSourcing\Aggregate\Event\Stream\EventStream;
use Zisato\EventSourcing\Aggregate\Event\Stream\GeneratorEventStream;
use Zisato\EventSourcing\Aggregate\Exception\AggregateReconstituteException;
use Zisato\EventSourcing\Aggregate\Exception\InvalidAggregateVersionException;
use Zisato\EventSourcing\Aggregate\ValueObject\Version;
use Zisato\EventSourcing\Identity\Identity;

abstract class AbstractAggregateRoot implements AggregateRoot
{
    use EventArgumentMethodCaller;

    private Identity $id;
    private Version $version;
    private EventStream $recordedEvents;

    final protected function __construct(Identity $id)
    {
        $this->id = $id;
        $this->version = Version::zero();
        $this->recordedEvents = GeneratorEventStream::create();
    }

    public static function reconstitute(Identity $id, EventStream $eventStream): AggregateRoot
    {
        if ($eventStream->isEmpty()) {
            throw new AggregateReconstituteException(\sprintf(
                'Cannot reconstitute aggregate from empty event stream'
            ));
        }

        $instance = new static($id);

        $instance->replyEvents($eventStream);

        return $instance;
    }

    public function id(): Identity
    {
        return $this->id;
    }

    public function version(): Version
    {
        return $this->version;
    }

    public function replyEvents(EventStream $eventStream): void
    {
        foreach ($eventStream->events() as $event) {
            $this->apply($event);

            $this->version = $event->aggregateVersion();
        }
    }

    public function releaseRecordedEvents(): EventStream
    {
        $recordedEvents = $this->recordedEvents;

        $this->recordedEvents = GeneratorEventStream::create();

        return $recordedEvents;
    }

    public function countRecordedEvents(): int
    {
        return $this->recordedEvents->count();
    }

    protected function recordAggregateEvent(Event $event): void
    {
        $nextVersion = $this->version->next();

        $event = $event->withAggregateVersion($nextVersion);

        $this->apply($event);

        $this->recordedEvents->add($event);

        $this->version = $nextVersion;
    }

    private function apply(Event $event): void
    {
        if (!$event->aggregateVersion()->equals($this->version()->next())) {
            throw new InvalidAggregateVersionException(\sprintf(
                'Cannot apply event %s with aggregate version %d, it must follow current aggregate version %s',
                \get_class($event),
                $event->aggregateVersion()->value(),
                $this->version->value()
            ));
        }

        $this->callMethod($event, 'apply');
    }
}
