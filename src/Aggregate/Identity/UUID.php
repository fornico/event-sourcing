<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Identity;

use Zisato\EventSourcing\Identity\GenerableIdentity;
use Zisato\EventSourcing\Identity\Identity;
use Ramsey\Uuid\Uuid as RamseyUuid;
use Ramsey\Uuid\UuidInterface;

class UUID implements GenerableIdentity
{
    private string $value;

    final private function __construct(UuidInterface $value)
    {
        $this->value = $value->toString();
    }

    public static function fromString(string $value): Identity
    {
        $uuid = RamseyUuid::fromString($value);

        return new self($uuid);
    }

    public static function generate(): Identity
    {
        $uuid = RamseyUuid::uuid1();

        return new self($uuid);
    }

    public function value(): string
    {
        return $this->value;
    }
}
