<?php

declare(strict_types=1);

namespace Zisato\EventSourcing\Aggregate\Identity\Factory;

use Zisato\EventSourcing\Aggregate\Identity\UUID;
use Zisato\EventSourcing\Identity\Factory\IdentityFactory;
use Zisato\EventSourcing\Identity\Identity;

class UUIDFactory implements IdentityFactory
{
    public function fromString(string $identity): Identity
    {
        return UUID::fromString($identity);
    }
}
