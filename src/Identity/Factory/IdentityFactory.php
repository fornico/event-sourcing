<?php

namespace Zisato\EventSourcing\Identity\Factory;

use Zisato\EventSourcing\Identity\Identity;

interface IdentityFactory
{
    public function fromString(string $identity): Identity;
}
