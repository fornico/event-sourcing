<?php

namespace Zisato\EventSourcing\Identity;

interface GenerableIdentity extends Identity
{
    public static function generate(): Identity;
}
