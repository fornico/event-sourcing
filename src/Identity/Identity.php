<?php

namespace Zisato\EventSourcing\Identity;

interface Identity
{
    public static function fromString(string $value): Identity;

    public function value(): string;
}
