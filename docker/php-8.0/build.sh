#!/usr/bin/env bash

container="$1"
shift

case "$container" in
  all)
    docker build -t registry.gitlab.com/zisato/event-sourcing:php-8.0-cli -f cli/Dockerfile .
    docker push registry.gitlab.com/zisato/event-sourcing:php-8.0-cli

    docker build -t registry.gitlab.com/zisato/event-sourcing:php-8.0-cli-pcov -f cli-pcov/Dockerfile .
    docker push registry.gitlab.com/zisato/event-sourcing:php-8.0-cli-pcov
  
    docker build -t registry.gitlab.com/zisato/event-sourcing:php-8.0-cli-xdebug -f cli-xdebug/Dockerfile .
    docker push registry.gitlab.com/zisato/event-sourcing:php-8.0-cli-xdebug
    ;;
  cli)
    docker build -t registry.gitlab.com/zisato/event-sourcing:php-8.0-cli -f cli/Dockerfile .
    docker push registry.gitlab.com/zisato/event-sourcing:php-8.0-cli
    ;;
  cli-pcov)
    docker build -t registry.gitlab.com/zisato/event-sourcing:php-8.0-cli-pcov -f cli-pcov/Dockerfile .
    docker push registry.gitlab.com/zisato/event-sourcing:php-8.0-cli-pcov
    ;;
  cli-xdebug)
    docker build -t registry.gitlab.com/zisato/event-sourcing:php-8.0-cli-xdebug -f cli-xdebug/Dockerfile .
    docker push registry.gitlab.com/zisato/event-sourcing:php-8.0-cli-xdebug
    ;;
  *)
    echo "First argument allowed values are: all, cli, cli-pcov, cli-xdebug"
    ;;
esac