# Event Sourcing

* [Installation](./docs/installation.md)

* [Usage](./docs/usage.md)

* [Persistence](./docs/persistence.md)

* [Snapshot](./docs/snapshot.md)

* [Upcast](./docs/upcast.md)



## Install dependencies
```
make install
```

## Execute tests
```
make test
make test-coverage
make test-xdebug
```

## Execute some tools
```
make infection
make phpcbf
make phpstan
make psalm
```